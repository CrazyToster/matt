__author__ = 'APopov'
from info import models
from django.forms import ModelForm


class BundleForm(ModelForm):
    class Meta:
        model = models.Bundle
        exclude = ('profile',)


class KeywordForm(ModelForm):

    class Meta:
        model = models.KeyWord
        exclude = ('profile',)

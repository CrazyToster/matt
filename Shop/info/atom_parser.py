__author__ = 'APopov'
import urllib2
from info.models import ShopItem
from info.models import KeyWord
import feedparser

def run(request):
    html = urllib2.urlopen(request)
    atom = feedparser.parse(html)

    a = []
    #initialize keyword
    key_word = KeyWord()
    key_word.key_word = "VCR2"
    key_word.execute = True
    KeyWord.save(key_word)

    for entry in atom.entries:
        item = ShopItem()

        #Initialize shop item from atom request
        item.price = entry.s_inventories
        item.desc = entry.s_description
        item.shop = entry.s_name
        item.title = entry.title
        item.keyword = key_word

        #check if item is already exist
        try:
            existing_item = ShopItem.objects.get(title=item.title)
        except ShopItem.DoesNotExist:
            existing_item = None

        if existing_item is not None:
            a.append(existing_item)
        else:
        #save to database
            ShopItem.save(item)
        #add to collection to deal without database request
            a.append(item)
    return a




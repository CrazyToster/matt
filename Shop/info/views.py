from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.conf.urls import patterns
from django.contrib import admin
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.template.context import RequestContext
from info.models import Bundle
from models import ShopItem
from models import KeyWord
from forms import BundleForm
from forms import KeywordForm
from user_profile.forms import ExcelForm
from user_profile.models import UserProfile
from utils import excel_export

def home(request):
    return render_to_response("home.html")

@login_required
def load_bundle(request):
    profile_id = request.user.id
    request_context = request.POST.copy()
    request_context['info_keyword.profile_id'] = profile_id
    form = BundleForm(request_context, request.FILES)
    if form.is_valid():
        link = form.save(commit=False)
        link.profile = UserProfile.objects.get(id = request.user.id)
        link.save()
        return HttpResponseRedirect('/main')
    else:
        return render_to_response("main.html", {'form': form},)

@login_required
def add_keyword(request):
    form = KeywordForm(request.GET)
    if form.is_valid():
        key_word = form.save(commit=False)
        key_word.profile = UserProfile.objects.get(id = request.user.id)
	key_word.save()
	return HttpResponseRedirect('/main')
    else:
        return render_to_response("main.html", {'keyword' : form,})

@login_required
def new_bundle(request):
    if request.method == 'POST': # If the form has been submitted...
        form = BundleForm(request.POST) # A form bound to the POST data
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect("/profile") # Redirect after POST
        else:
            return render_to_response("new_bundle.html", {'form': form,})
    else:
        form = BundleForm()
    return render_to_response("new_bundle.html", {
    'form': form,
    })

@login_required
def bundle_context(request):
    bundle_context = Bundle.objects.get(id = request.GET['bundle'])
    keys = KeyWord.objects.filter(bundle = bundle_context)
    key_query = ShopItem.objects.filter(keyword__in = keys)
    eBay = ShopItem.objects.filter(shop__contains = 'eBay', keyword__in = keys)

    keywordForm = KeywordForm({'bundle':bundle_context})
    excelForm = ExcelForm()

    id = request.user.id
    profile = UserProfile.objects.get(id = id)
    lowest, not_lowest = ShopItem.get_related_items(profile.site)
    return render_to_response("bundle_context.html", {'user':request.user, 'lowest':lowest, 'not_lowest':not_lowest,
                              'keyquery' : key_query, 'excel_form' : excelForm, 'eBay' : eBay, 'bundle':bundle_context,
                              'keyword' : keywordForm,}, context_instance = RequestContext(request))

@login_required
def delete_bundle(request):
    bundle = Bundle.objects.get(id = request.GET['bundle'])

    if bundle.profile.id == request.user.id:
        bundle.delete()
        return HttpResponseRedirect('main')
    else:
        return HttpResponseRedirect('/error')



def export_view(request):
# Create the HttpResponse object with the appropriate CSV header.
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=result.xls'

    ids = request.GET['ids']
    items = ShopItem.objects.order_by('keyword__id').filter(pk__in = ids.split(','))

    wb = excel_export.exp(items)

    wb.save(response)
    return response

#CODE MARKED FOR REFACTOR
def next_item(request):
    pre_url = request.META['HTTP_REFERER']
    arr = pre_url.strip().split('/')
    if arr[-3] == 'shopitem':
        id = ShopItem.objects.get(id = arr[-2]).get_next()
        if id:
            return HttpResponseRedirect('/admin/info/shopitem/%s'%id)
        else:
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
    elif arr[-3] == 'keyword':
        id = KeyWord.objects.get(id = arr[-2]).get_next()
        if id:
            return HttpResponseRedirect('/admin/info/keyword/%s'%id)
        else:
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
    return HttpResponse(arr[-2])

#CODE MARKED FOR REFACTOR
def prev_item(request):
    pre_url = request.META['HTTP_REFERER']
    arr = pre_url.strip().split('/')
    if arr[-3] == 'shopitem':
        id = ShopItem.objects.get(id = arr[-2]).get_prev()
        if id:
            return HttpResponseRedirect('/admin/info/shopitem/%s'%id)
        else:
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
    elif arr[-3] == 'keyword':
        id = KeyWord.objects.get(id = arr[-2]).get_prev()
        if id:
            return HttpResponseRedirect('/admin/info/keyword/%s'%id)
        else:
            return HttpResponseRedirect(request.META['HTTP_REFERER'])
    return HttpResponse(arr[-2])

def get_admin_urls(urls):
    def get_urls():
        my_urls = patterns('',
            (r'^info/next/$', admin.site.admin_view(next_item)),
            (r'^info/prev/$', admin.site.admin_view(prev_item))
        )
        return my_urls + urls
    return get_urls

admin_urls = get_admin_urls(admin.site.get_urls())
admin.site.get_urls = admin_urls



from io import open
import os
import urllib
import codecs
import urllib2
from django.db.models.signals import post_save
import feedparser
from django.core.exceptions import ValidationError
from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.http import HttpResponseRedirect, HttpResponse
from Shop import settings
import datetime
from user_profile.models import UserProfile
from utils import excel_export, reg_exp


def validate_file_extension(value):
    if not value.name.endswith('.txt'):
        raise ValidationError(u'Only txt files allowed')

class Bundle(models.Model):
    name = models.BooleanField(blank = True, null = True)
    name = models.CharField(max_length=250)
    key_word_list = models.FileField(upload_to=settings.TMP_DIR, validators=[validate_file_extension])
    profile = models.ForeignKey(UserProfile, blank = True, null = True)

    def get_file(self):
        return self.key_word_list.__str__()

    def get_profile(self):
        return self.profile

    def update_bundle(self):
        objects = KeyWord.objects.filter(bundle = self)
        for obj in objects:
            url_encode = urllib.quote_plus(obj.key_word.__str__())
            request = settings.GOOGLE_URL+"?key=AIzaSyBg_CB-7O9BHeS-NNrTHRQDoUFLClZ2OEY&country=US&q=%s&alt=atom"%url_encode
            run(request, id)

    def __str__(self):
        return self.name


class KeyWord(models.Model):
    key_word = models.CharField(max_length=250)
    execute = models.BooleanField()
    profile = models.ForeignKey(UserProfile, blank=True, null=True)
    bundle = models.ForeignKey(Bundle)

    def get_next(self):
        next = KeyWord.objects.order_by('id').filter(id__gt = self.id)
        if next:
            return next[0].id
        else:
            return False

    def get_prev(self):
        next = KeyWord.objects.order_by('-id').filter(id__lt = self.id)
        if next:
            return next[0].id
        else:
            return False

    def __str__(self):
        return self.key_word.encode('utf-8')


def create_bundle_callback(sender, instance, **kwargs):
    path = instance.get_file()
    with codecs.open(path, "r", "utf-8") as key_words:
        for line in key_words:
            KeyWord.objects.get_or_create(key_word=line, profile = instance.profile, bundle = instance, execute = True)
    os.remove(path)

def create_keyword_callback(sender, instance, **kwargs):
    url_encode = urllib.quote_plus(instance.key_word.__str__())
    request = settings.GOOGLE_URL+"?key=AIzaSyBg_CB-7O9BHeS-NNrTHRQDoUFLClZ2OEY&country=US&q=%s&alt=atom"%url_encode
    run(request, instance)

class BundleAdmin(admin.ModelAdmin):
    list_display = ['key_word_list']

class ShopItem(models.Model):
    title = models.CharField(max_length=250)
    desc = models.TextField()
    price = models.FloatField()
    shop = models.CharField(max_length=250)
    date = models.DateField()
    keyword = models.ForeignKey(KeyWord)
    url = models.URLField()

    @classmethod
    def get_related_items(cls, shop):
        lowest = []
        not_lowest = []
        items = ShopItem.objects.order_by('keyword', 'price').all()

        if shop is None:
            return lowest, not_lowest

        if len(items):
            keyword = items[0].keyword

            if reg_exp.find(shop.lower(), items[0].shop.lower()):
                lowest.append(items[0])

            for item in items[1:]:
                if keyword != item.keyword and reg_exp.find(shop.lower(), item.shop.lower()):
                    lowest.append(item)
                    keyword = item.keyword
                    continue
                if keyword == item.keyword and reg_exp.find(shop.lower(), item.shop.lower()):
                    not_lowest.append(item)
                    keyword = item.keyword

        return lowest, not_lowest

    def get_next(self):
        next = ShopItem.objects.order_by('id').filter(id__gt = self.id)
        if next:
            return next[0].id
        else:
            return False

    def get_prev(self):
        next = ShopItem.objects.order_by('-id').filter(id__lt = self.id)
        if next:
            return next[0].id
        else:
            return False

    def __str__(self):
        return self.price.__str__() + " - " + self.title.encode('utf-8')

    def __excel__(self):
        return self.price.__str__() + " - " + self.shop.encode('utf-8')

    def __init__(self, *args, **kwargs):
        super(ShopItem, self).__init__(*args, **kwargs)
        self.date = datetime.datetime.now()

    class Meta:
        ordering = ["price"]

def export_selected_objects(modeladmin, request, queryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    ct = ContentType.objects.get_for_model(queryset.model)
    return HttpResponseRedirect("/export/?ct=%s&ids=%s" % (ct.pk, ",".join(selected)))

def export_all(modeladmin, request, queryset):
    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=result.xls'

    items = ShopItem.objects.all()

    wb = excel_export.exp(items)

    wb.save(response)
    return response



def refresh_keyword_results(medladmin, request, quryset):
    selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
    words_set = KeyWord.objects.filter(pk__in = selected)
    for word in words_set:
        url_encode = urllib.quote_plus(word.__str__())
        request = settings.GOOGLE_URL+"?key=AIzaSyBg_CB-7O9BHeS-NNrTHRQDoUFLClZ2OEY&country=US&q=%s&alt=atom"%url_encode
        run(request, word)
        # run keywords from list
    return HttpResponseRedirect("/admin/info/shopitem")

class KeyWordAdmin(admin.ModelAdmin):
    actions = [refresh_keyword_results]

class ShopItemAdmin(admin.ModelAdmin):
    list_display = ('keyword', 'title', 'shop' ,'date', 'price')
    actions = [export_selected_objects]
    list_filter = ('price', 'date', 'title', 'keyword')

def run(request, id):
    html = urllib2.urlopen(request)
    atom = feedparser.parse(html)

    a = []
    for entry in atom.entries:
        item = ShopItem()

        #Initialize shop item from atom request
        item.price = entry.s_inventories.encode('utf-8')
        item.desc = entry.s_description.encode('utf-8')
        item.shop = entry.s_name.encode('utf-8')
        item.title = entry.title.encode('utf-8')
        item.url =  entry.s_link.encode('utf-8')
        item.keyword = id

        #save to database
        ShopItem.save(item)
            #add to collection to deal without database request
        a.append(item)
    return a

admin.site.register(ShopItem, ShopItemAdmin)
admin.site.register(KeyWord, KeyWordAdmin)
admin.site.register(Bundle, BundleAdmin)
post_save.connect(create_bundle_callback, sender = Bundle)
post_save.connect(create_keyword_callback, sender = KeyWord)

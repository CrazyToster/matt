function get_status(num)
{
    switch(num)
    {
        case "Normal":
            $(".priority").css("color", "#46a546");
            break;
        case "High":
            $(".priority").css("color", "#049cdb");
            break;
        case "Urgent":
            $(".priority").css("color", "#9d261d");
            break;
        case "Immediate":
            $(".priority").css("color", "#ffc40d");
            break;
        case "Failed":
            $(".priority").css("color", "#049cdb");
            break;
        case "Succeed":
            $(".priority").css("color", "#049cdb");
            break;
    }
}

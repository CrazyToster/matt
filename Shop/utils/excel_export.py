__author__ = 'APopov'

import xlwt


def exp(items):
    font = xlwt.Font()
    font.name = 'Times New Roman'
    font.bold = True

    style = xlwt.XFStyle()
    style.font = font

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('A Test Sheet')

    ws.write(0, 0, "Title", style)
    ws.write(0, 1, "KeyWord", style)
    ws.write(0, 2, "Company", style)
    ws.write(0, 3, "ShopPrice", style)
    ws.write(0, 4, "URL", style)	

    #NEED TO REFACTOR RECREATE ALGORITHM
    prev = items[0].keyword_id
    j = 1

    for item in items[1:]:
        ws.write(j, 0, item.title, style)
        ws.write(j, 1, item.__excel__(), style)
        ws.write(j, 2, item.shop, style)
        ws.write(j, 3, item.price, style)
    	ws.write(j, 4, item.url, style)
	j+=1
        if item.keyword_id != prev:
            prev = item.keyword_id
    return wb

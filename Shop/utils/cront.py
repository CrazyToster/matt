__author__ = 'APopov'
import os
import sys, traceback
path = '/home/user1/Shop'
if path not in sys.path:
    sys.path.insert(0, '/home/user1/Shop')

os.environ['DJANGO_SETTINGS_MODULE'] = 'Shop.settings'

from info import models

def update():
    bundles = models.Bundle.objects.all()

    for bundle in bundles:
	print bundle.get_file()
	try:
		bundle.update_bundle()
	except:
        	print "Exception in user code:"
       		print '-'*60
       		traceback.print_exc(file=sys.stdout)
        	print '-'*60
update()


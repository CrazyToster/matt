import os
import sys
 
path = '/home/user1/matt/Shop'
if path not in sys.path:
    sys.path.insert(0, '/home/user1/matt/Shop')
 
os.environ['DJANGO_SETTINGS_MODULE'] = 'Shop.settings'
 
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()


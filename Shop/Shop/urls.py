from django.conf.urls import patterns, include, url
from django.contrib import admin
from info.views import export_view, home
from info.views import new_bundle
from user_profile.views import registration
from user_profile.views import profile
from user_profile.views import main
from user_profile.views import logout_view
from user_profile.views import login_view
from info.views import load_bundle
from info.views import add_keyword
from user_profile.views import excel
from info.views import bundle_context
from info.views import delete_bundle



# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^add_keyword', add_keyword),
    url(r'^load_bundle',load_bundle),
    url(r'^registration$', registration),
    url(r'^profile$', profile),
    url(r'^main', main),
    url(r'^export/$', export_view),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^new_bundle$',new_bundle),
    url(r'^logout$', logout_view),
    url(r'^home', home),
    url(r'^excel', excel),
    url(r'bundle_context', bundle_context),
    url(r'delete', delete_bundle),
    (r'^login$', login_view),
#    url(r'^new_keyword$',),
)

__author__ = 'Diablo'
import os

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'tmp')

TEMPLATE_DIRS = os.path.join(PROJECT_ROOT,'templates')

TMP_DIR = os.path.join(PROJECT_ROOT, 'tmp')

print PROJECT_ROOT, MEDIA_ROOT, TEMPLATE_DIRS, TMP_DIR
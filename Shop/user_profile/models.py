from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    site = models.CharField(max_length=250, null=True, blank=True)

    def __unicode__(self):
        return self.user.username

def create_user_callback(sender, instance, **kwargs):
    user_profile, new = UserProfile.objects.get_or_create(user = instance)
post_save.connect(create_user_callback, User)

admin.site.register(UserProfile)
admin.site.register(User)
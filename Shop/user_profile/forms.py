__author__ = 'APopov'
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from user_profile.models import UserProfile


class ExcelForm(forms.Form):
    date_from = forms.DateField()

class ProfileForm(ModelForm):
    site = forms.CharField(label='Your site in google')

    class Meta:
        model = UserProfile
        exclude = ('user')

class RegistrationForm(ModelForm):
    email = forms.EmailField(label="Email")

    class Meta:
        model = User
        exclude = ('is_staff', 'is_active', 'last_login', 'date_joined', 'is_superuser', 'user_permissions', 'groups')





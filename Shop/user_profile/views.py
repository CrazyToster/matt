from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from info.forms import BundleForm, KeywordForm
from info.models import ShopItem, Bundle, KeyWord
from forms import RegistrationForm
from forms import ProfileForm
from user_profile.forms import ExcelForm
from user_profile.models import UserProfile
from utils import excel_export

def excel(request):
    get_request = request.GET
    date = get_request['date_from']
    pre_url = request.META['HTTP_REFERER']
    arr = pre_url.strip().split('?')[1].split('=')
    words = KeyWord.objects.filter(bundle = arr[1])
    items = ShopItem.objects.filter(date__gt = date, keyword__in = words)

    response = HttpResponse(mimetype="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=result.xls'

    wb = excel_export.exp(items)

    wb.save(response)
    return response

def registration(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/profile')
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.cleaned_data['password_submit']
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            email=form.cleaned_data['email'],
                                            password=form.cleaned_data['password'])
            user_profile = user.get_profile()
            user_profile.save()
            return HttpResponseRedirect("/main")
        else:
            return render_to_response("registration.html", {"form" : form,}, context_instance = RequestContext(request))
    else:
        '''user is not submitting the form, show a blank registration form '''
        form = RegistrationForm()
        context = {'form' : form,}
        return render_to_response('registration.html', context, context_instance = RequestContext(request))

@login_required
def profile(request):
    if request.method == 'POST':
        reg_form = RegistrationForm(request.POST, instance=request.user)
        pro_form = ProfileForm(request.POST, instance=UserProfile.objects.get(id = request.user.id))
        if reg_form.is_valid() and pro_form.is_valid():
            reg_form.save()
            pro_form.save()
        else:
            return render_to_response("auth.html",  {'reg_form': reg_form, 'pro_form':pro_form}, context_instance = RequestContext(request))
        return HttpResponseRedirect("/main")
    else:
        if request.user.is_authenticated:
            user = request.user
            pro = UserProfile.objects.get(id = user.id)
            reg_form = RegistrationForm(instance=user)
            pro_form = ProfileForm(instance=pro)
            return  render_to_response("auth.html",  {'reg_form': reg_form, 'pro_form':pro_form},
                                       context_instance = RequestContext(request))
        reg_form = RegistrationForm()
        pro_form = ProfileForm()
        return render_to_response("auth.html",  {'reg_form': reg_form, 'pro_form':pro_form}, context_instance = RequestContext(request))

@login_required
def main(request):
    if request.user.is_authenticated:
        bundleForm = BundleForm()
        bundles = Bundle.objects.all()
        id = request.user.id
        profile = UserProfile.objects.get(id = id)
        lowest, not_lowest = ShopItem.get_related_items(profile.site)
        return render_to_response("profile.html", {'msg' : 'You are already login', 'user':request.user, 'lowest':lowest, 'not_lowest':not_lowest,
                                                   'bundle' : bundleForm, 'bundles':bundles,}, context_instance = RequestContext(request))


def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/home')

def login_view(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/main')
            else:
                return render_to_response('login.html', {'msg': "Invalid username or password"}, context_instance = RequestContext(request))
        else:
            return render_to_response('login.html')
    else: return render_to_response("login.html", context_instance = RequestContext(request))


